#include <iostream>
#include <math.h>
#include <cstdlib>
#include <time.h>
#include "LabFunction.h"

LabFunction::LabFunction()
{
    srand((unsigned)time(NULL));

    const int MIN = -100, MAX = 100;

    double k;

    k = ((double)rand()) / RAND_MAX;
    this->a = k * (MAX - MIN) + MIN;

    k = ((double)rand()) / RAND_MAX;
    this->b = k * (MAX - MIN) + MIN;
    if (this->b == 0)
    {
        this->b += k;
    }

    k = ((double)rand()) / RAND_MAX;
    this->c = k * (MAX - MIN) + MIN;

    k = ((double)rand()) / RAND_MAX;
    this->d = k * (MAX - MIN) + MIN;

    k = ((double)rand()) / RAND_MAX;
    this->f = k * (MAX - MIN) + MIN;

    this->isCalculusAffected = false;
}

std::string LabFunction::show() const
{
    std::string function = "f(x) = ";

    if (this->a == 0 && this->d == 0)
    {
        return function + "0";
    }

    if (this->a != 0)
    {
        function += std::to_string(a) + " * sh(";
        if (this->b == 0)
        {
            function += std::to_string(this->c) + ")";
        }
        else
        {
            if (this->c > 0)
            {
                function += std::to_string(this->b) + " * x + " + std::to_string(this->c) + ")";
            }
            else if (c < 0)
            {
                function += std::to_string(this->b) + " * x " + std::to_string(this->c) + ")";
            }
            else
            {
                function += std::to_string(b) + "* x)";
            }
        }
    }
    
    if (this->d != 0)
    {
        if (d > 0)
        {
            function += " + " + std::to_string(this->d);
        }       
        else if (d < 0)
        {
            function += " " + std::to_string(this->d);
        }

        function += " * ch(e * x ";

        if (f > 0)
        {
            function += "+ " + std::to_string(this->f) + ")";
        }
        else if (f < 0)
        {
            function += std::to_string(this->f) + ")";
        }
        else
        {
            function += ")";
        }
    }

    if (this->isCalculusAffected)
    {
        function.replace(function.find("ch"), 2, "sh");
        function.replace(function.find("sh"), 2, "ch");               
    }
    else if (!this->isCalculusAffected)
    {
        function.replace(function.find("sh"), 2, "ch");
        function.replace(function.find("ch"), 2, "sh");
    }
    
    return function;
}

void LabFunction::differentiate()
{
    const double e = 2.71828;

    this->a *= this->b;
    this->d *= e;
    this->isCalculusAffected = !this->isCalculusAffected;
}

void LabFunction::integrate()
{
    const double e = 2.71828;
    
    if (this->b == 0)
    {
        std::cout << "Seems like b is 0, so you can't integrate this functiondue to division by 0" << std::endl;
        return;
    }
    
    this->a /= this->b;
    this->d /= e;
    this->isCalculusAffected = !this->isCalculusAffected;
}

double LabFunction::operator()(const double x) const
{
    const double e = 2.71828;
    
    if (this->isCalculusAffected)
    {
        return this->a * cosh(this->b * x + this->c) + d * sinh(e * x + this->f);
    }
    else
    {
        return this->a * sinh(this->b * x + this->c) + d * cosh(e * x + this->f);
    }
    
}

LabFunction LabFunction::operator/=(const double k)
{
    a /= k;
    d /= k;
    
    return *this;
}

LabFunction LabFunction::operator/(const double k) const
{
    LabFunction temp = *this;

    return temp /= k;
}

LabFunction LabFunction::operator*=(const double k)
{
    a *= k;
    d *= k;
    
    return *this;
}

LabFunction LabFunction::operator*(const double k) const
{
    LabFunction temp = *this;

    return temp *= k;
}