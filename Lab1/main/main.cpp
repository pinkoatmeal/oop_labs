﻿#include <iostream>
#include <string>
#include "LabFunction.h"

using namespace std;

int main()
{
    LabFunction a, b(2.3, 5.6, 3.3, 40, 20.98), c(9, 11), d(true);   
    
    cout << "Functions:" << endl;
    cout << a.show() << endl;
    cout << b.show() << endl;
    cout << c.show() << endl;
    cout << d.show() << endl;
    cout << endl;
    
    cout << "Division of a:" << endl;
    a /= 9;
    cout << a.show() << endl;
    cout << endl;
    
    cout << "Multiplication of b:" << endl;
    b *= 3;
    cout << b.show() << endl;
    cout << endl;

    cout << "Integrated c:" << endl;
    c.integrate();
    cout << c.show() << endl;
    

    return 0;
}
