#pragma once

#include <string>

class LabFunction
{
public:	
    LabFunction();

	LabFunction(double a, double b, double c, double d, double f, bool state = false): a(a), b(b), c(c), d(d), f(f), isCalculusAffected(state)
	{
		
	}

    LabFunction(double a, double d): LabFunction(a, 3, 4, d, 5, false)
    {

    }

    LabFunction(bool state) : LabFunction(2, 3, 4, 5, state)
    {

    }

	double get_a() const 
	{
		return this->a;
	}

	void set_a(double a)
	{
		this->a = a;
	}

	double get_b() const
	{
		return this->b;
	}

	void set_b(double b)
	{
		this->b = b;
	}

	double get_c() const
	{
		return this->c;
	}

	void set_c(double c)
	{
		this->c = c;
	}

	double get_d() const
	{
		return this->d;
	}

	void set_d(double d)
	{
		this->d = d;
	}

	double get_f() const
	{
		return this->f;
	}

	void set_f(double f)
	{
		this->f = f;
	}

    bool get_calculus_state() const
    {
        return this->isCalculusAffected;
    }

    void set_calculus_state(bool state)
    {
        this->isCalculusAffected = state;
    }

    double operator()(const double x) const;

    std::string show() const;

    void differentiate();

    void integrate();
    
	LabFunction operator/=(double k);

    LabFunction operator/(const double k) const;

    LabFunction operator*=(double k);

    LabFunction operator*(const double k) const;

private:
    double a;
    double b;
    double c;
    double d;
    double f;
    bool isCalculusAffected;
};
