﻿#include <iostream>
#include "MyArray.h"

using namespace std;

int main()
{
    MyArray<double> kek(7);
    
    kek[0] = 11;
    kek[1] = 0;
    kek[2] = 8;
    kek[3] = 22;
    kek[4] = 11;
    kek[5] = -23;
    kek[6] = 0.1;


    for (int i = 0; i < kek.size(); i++)
    {
            cout << "array[" << i << "] = " << kek[i] << endl;
    }

    kek.resize(3);
    
    cout << "==============================" << endl;

    for (int i = 0; i < kek.size(); i++)
    {
        cout << "array[" << i << "] = " << kek[i] << endl;
    }

    return 0;
}
