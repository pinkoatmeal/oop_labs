#pragma once

template <typename TYPE>
class MyArray
{
public:
    MyArray(): _size(0), _array(nullptr)
    {

    }
    MyArray(int _size): _size(_size)
    {
        _array = new TYPE[_size];
    }
    MyArray(int _size, TYPE value): _size(_size)
    {
        _array = new TYPE[_size];
        for (int i = 0; i < _size; i++)
        {
            _array[i] = value;
        }
    }
    MyArray(MyArray<TYPE>& original)
    {
        copyMethod(original);
    }
    ~MyArray()
    {
        delete _array;
    }

    TYPE& operator[](int index);

    int size() const;

    void copyMethod(MyArray<TYPE>& original);
    MyArray<TYPE> operator=(MyArray<TYPE>& original);
    void resize(int interval);

private:
    int _size;
    TYPE* _array;
};

template <typename TYPE>
TYPE& MyArray<TYPE>::operator[](int index)
{
    return this->_array[index];
}

template <typename TYPE>
int MyArray<TYPE>::size() const 
{
    return _size;
}

template <typename TYPE>
void MyArray<TYPE>::copyMethod(MyArray<TYPE>& original)
{
    this->_size = original.size();
    this->_array = new TYPE[this->_size];

    for (int i = 0; i < this->_size; i++)
    {
        this->_array[i] = original[i];
    }
}

template <typename TYPE>
MyArray<TYPE> MyArray<TYPE>::operator=(MyArray<TYPE>& original)
{
    if (this->_array != original._array)
    {
        this->~MyArray();
        copyMethod(original);
    }
    return *this;
}

template <typename TYPE>
void MyArray<TYPE>::resize(int interval)
{
    int minIndex;
    double avg, minAvg, sum = 0;

    for (int i = 0; i < interval; i++)
    {
        sum += this->_array[i];
    }

    minAvg = sum / interval;

    for (int i = 0; i <= this->_size - interval; i++)
    {
        sum = 0;

        for (int j = i; j < i + interval; j++)
        {
            sum += this->_array[j];
        }

        avg = sum / interval;

        if (avg < minAvg)
        {
            minAvg = avg;
            minIndex = i;
        }
    }

    MyArray<TYPE> temp(this->_size - interval);

    for (int i = 0; i < minIndex; i++)
    {
        temp[i] = this->_array[i];
    }

    for (int i = minIndex; i < temp.size(); i++)
    {
        temp[i] = this->_array[i];
    }

    *this = temp;
}